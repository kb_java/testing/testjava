package com.eichtec.TestJava.player;

import java.util.Random;

//dado
public class Dice {
    //nro de caras
    private int sides;

    public Dice(int sides) {
        this.sides = sides;
    }

    public int roll(){

        return new Random().nextInt(sides) + 1;
    }
}
