package com.eichtec.TestJava.player;

public class Player {

    private Dice dice;
    private int minMumberToWin;

    public Player(Dice dice, int minMumberToWin) {
        this.dice = dice;
        this.minMumberToWin = minMumberToWin;
    }

    public boolean play(){
        int diceNumber = dice.roll();
        return diceNumber > minMumberToWin;
    }
}
