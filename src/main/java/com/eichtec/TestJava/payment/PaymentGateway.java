package com.eichtec.TestJava.payment;

public interface PaymentGateway {
    PaymentResponse requestPayment(PaymentRequest request);
}
